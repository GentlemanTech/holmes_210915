FROM golang as buildserver
WORKDIR /build
COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY src ./src
RUN CGO_ENABLED=0 go build -o webserver.bin ./src

FROM alpine as webserver
RUN apk add --no-cache ffmpeg
COPY --from=buildserver /build/webserver.bin /usr/bin/
RUN mkdir -p /var/webserver/templates
RUN mkdir -p /var/webserver/public
COPY ./templates /var/webserver/templates
COPY ./public /var/webserver/public
EXPOSE 80
WORKDIR /var/webserver
ENTRYPOINT [ "/usr/bin/webserver.bin" ]

