Vue.component("video-list", {
	template: `
		<div>
			<p>Uploaded videos are:</p>
			<table class="video-list">
				<tr>
					<th>Name</th>
					<th>Thumbnail</th>
					<th>Category</th>
				</tr>
				<video-item v-for="(video, index) in videos" :video="video" @showvideo="showvideo">
				</video-item>
			</table>
		</div>

	`,
	props: {
		videos: {
			type: Array,
			required: true,
		}
	},
	methods: {
		showvideo(video) {
			this.$emit("showvideo", video);
		}
	}
});

Vue.component("video-item", {
	template: `
		<tr @click="showvideo" >
			<td>{{video.Name}}</td>
			<td><img :src="thumbnailURL" class="thumbnail" :title="video.Name"></td>
			<td>{{video.Category}}</td>
		</tr>
	`,
	props: {
		video: {
			type: Object,
			required: true,
		}
	},
	methods: {
		showvideo() {
			this.$emit("showvideo", this.video);
		}
	},
	computed: {
		thumbnailURL() {
			return "/thumbnail/" + this.video.Thumbnail256File;
		}
	}
});

