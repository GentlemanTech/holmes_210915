Vue.component("video-uploader", {
	template: `
		<form @submit.prevent>
			<div class="formfield">
				<label for="title">What is the title of your video:</label>
				<input type="text" v-model="title">
			</div>
			<div class="formfield">
				<label for="category">What category does your video fall into:</label>
				<select v-model="category">
					<option disabled value="">please choose a category</option>
					<option v-for="cat in categories" :value="cat">{{cat}}</option>
				</select>
			</div>
			<div class="formfield">
				<label for="file">Select your file to upload:</label>
				<input type="file" name="file" accept=".mov,.mp4" @change="addFiles">
			</div>
			<div class="formfield" v-if="error_message != ''">
				<p class="error">{{error_message}}</p>
			</div>
			<div class="formfield">
				<button v-if="!uploading" @click.stop.prevent="validate">Upload</button>
				<progress value="progress" max="progress_max" v-if="uploading"></progress>
			</div>
		</form>
	`,
	props: {
		categories: {
			type: Array,
			required: true,
		},
	},
	data() {
		return {
			title: "",
			category: "",
			file: {},
			error_message: "",
			progress: 0,
			progress_max: 0,
			uploading: false,
		}
	},
	methods: {
		validate() {
			console.log("validating");
			this.error_message = "";
			if (this.title == "") {
				this.error_message = "the video needs a title please";
				return;
			}
			if (!this.file) {
				this.error_message = "please select a file to upload";
				return
			}
			if (this.category == "") {
				this.error_message = "please select a category for the video";
				return;
			}
			this.upload();
		},
		addFiles(evt) {
			console.log("adding files");
			var elem = evt.target;
			var new_files = elem.files;
			if (new_files.length != 1) {
				this.error_message = "please select a single video file";
				return
			}
			this.file = new_files[0];
			evt.target.value = ""
		},
		upload() {
			console.log("uploading");
			let self = this;
			let xhr = new XMLHttpRequest();
			var fd = new FormData();
			fd.append("file", self.file, self.file.name);
			fd.append("mimetype", self.file.type);
			fd.append("category", self.category);
			fd.append("title", self.title);

			xhr.open("POST", "/file_upload/", true);
			xhr.onreadystatechange = function (evt) {
				if (xhr.readyState == 4) {
					if (xhr.status == 200) {
						//all good, redirect to index
						window.location.href = "/";
						return;
					}
					self.error_message = xhr.statusText;
					return;
				}
			}
			xhr.onerror = function (evt) {
				self.error_message = evt.statusText;
				self.uploading = false;
				self.progress = 0;
				return;
			}
			xhr.upload.addEventListener("progress", function (evt) {
				self.progress = evt.loaded;
			});
			xhr.send(fd);
		}
	}
});