Vue.component("video-player", {
	template: `
		<video controls class="video-player" @ended="stopVideo" autoplay>
		    <source :src="videoname">
    		Sorry, your browser doesn't support embedded videos.
		</video>
	`,
	props: {
		video: {
			type: Object,
			required: true,
		}
	},
	data() {
		return {};
	},
	methods: {
		stopVideo() {
			this.$emit("stopVideo");
		},
	},
	computed: {
		videoname() {
			return "/video/" + this.video.ID;
		},
		videoID() {
			return this.video.ID;
		}
	}

});