Vue.component("test-component", {
	template: `
		<div>Message is: {{message}}</div>
	`,
	props: {
		message: {
			type: String,
			required: true,
		}
	},
});