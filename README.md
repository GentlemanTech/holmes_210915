# HOLMES_210915

Technical test exercise for *secret* recruitment

## How to build:

I use a Makefile for convenience
`make dev_database` will run the database on a local docker instance
`make run_migrations` will initialise the database
`make run` will run the webserver in the terminal

there is also a docker-compose.yml that will set up the servers in docker instances.
However, I run Linux so I have no idea if this will actually work on a Mac