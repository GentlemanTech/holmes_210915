package main

import (
	"fmt"
	"os"

	"gitlab.com/GentlemanTech/holmes_210915/src/database"
	"gitlab.com/GentlemanTech/holmes_210915/src/webserver"
)

func main() {
	//connect to the database
	db, err := database.ConnectToDB("localhost", "localdev")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	//set up the file store
	webserver.SetFileStoreDir("./testdata/files")

	//run the webserver
	err = webserver.Run(db, "localhost:8080", "templates", "public")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	os.Exit(0)
}
