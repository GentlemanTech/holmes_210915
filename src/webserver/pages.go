package webserver

import (
	"fmt"
	"html/template"
	"path/filepath"
)

//using a module-scoped variable because it's easier/quicker for this exercise,
//   though generally I prefer not using them and would create a struct
var tmps *template.Template

func LoadPageTemplates(templatedir string) error {
	var err error
	tmps = template.New("").Delims("[[", "]]")
	tmps, err = tmps.ParseGlob(filepath.Join(templatedir, "*.gohtml"))
	if err != nil {
		return fmt.Errorf("failed to parse templates: %w", err)
	}
	fmt.Println("loaded page templates successfully")
	return nil
}
