package webserver

import (
	"io"
	"net/http"
	"strings"
	"testing"
	"time"

	"gitlab.com/GentlemanTech/holmes_210915/src/database"
)

//ideally I should split these tests out to their various Go source files
// but they're so interlinked...meh

func TestWebServer(t *testing.T) {
	db := openTestDatabase(t)
	go Run(db, "http://localhost:8080", "../../testdata/templates", "../../testdata/assets")
	//let the templates load, etc
	time.Sleep(time.Millisecond * 100)
	r, err := http.DefaultClient.Get("http://localhost:8080")
	if err != nil {
		t.Fatal("failed with: ", err)
	}
	b, err := io.ReadAll(r.Body)
	if err != nil {
		t.Fatal("failed to read body")
	}
	if !strings.Contains(string(b), "test") {
		t.Fatal("failed to get a test result", string(b))
	}
}

func openTestDatabase(t *testing.T) *database.VideoData {
	result, err := database.ConnectToDB("localhost", "localtest")
	if err != nil {
		t.Fatal("failed to open database: ", err)
	}
	return result
}
