package webserver

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"

	"github.com/google/uuid"
	"gitlab.com/GentlemanTech/holmes_210915/src/models"
)

var testFileStoreDir = "../../testdata/files"

func TestFileSave(t *testing.T) {
	video := models.Video{
		ID:       uuid.New(),
		Name:     "test video storage",
		Filename: "badgers.mp4",
		Category: "exercise",
	}
	SetFileStoreDir(testFileStoreDir)
	path, err := filepath.Abs("../../testdata/badgers.mp4")
	if err != nil {
		t.Fatal("unable to get test file path")
	}
	srci, err := os.Stat(path)
	if err != nil {
		t.Fatal("failed to stat source file: ", err)
	}
	file, err := os.Open(path)
	if err != nil {
		t.Fatal("unable to open test file:", err)
	}
	defer file.Close()
	filename, err := storeFile(video, file)
	if err != nil {
		t.Fatal("failed to store file: ", err)
	}
	if filename == "" {
		t.Fatal("failed to return a filename")
	}
	dst := filepath.Join(fileStoreDir, filename)
	fi, err := os.Stat(dst)
	if err != nil {
		t.Fatal("failed to stat the stored file: ", err)
	}
	defer os.Remove(dst)
	if fi.Size() != srci.Size() {
		t.Fatal("file size on disk: ", fi.Size(), "doesn't match file copy size: ", srci.Size())
	}
	//assume all good then
}

func TestFileUpload(t *testing.T) {

	video := models.Video{
		ID:       uuid.New(),
		Name:     "test video storage",
		Filename: "badgers.mp4",
		Category: "exercise",
	}
	SetFileStoreDir(testFileStoreDir)
	path, err := filepath.Abs("../../testdata/badgers.mp4")
	if err != nil {
		t.Fatal("unable to get test file path")
	}
	file, err := os.Open(path)
	if err != nil {
		t.Fatal("unable to open test file:", err)
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	defer writer.Close()
	part, err := writer.CreateFormFile("file", filepath.Base(path))
	if err != nil {
		t.Fatal("unable to create form file: ", err)
	}
	_, err = io.Copy(part, file)
	if err != nil {
		t.Fatal("failed to copy part:", err)
	}
	mimetype := writer.FormDataContentType()
	file.Close()

	err = writer.WriteField("title", video.Name)
	if err != nil {
		t.Fatal("failed to write title")
	}
	err = writer.WriteField("category", video.Category)
	if err != nil {
		t.Fatal("failed to write category")
	}
	writer.Close()

	r, err := http.NewRequest("POST", "/file_upload/", body)
	if err != nil {
		t.Fatal("Unable to create request: ", err)
	}
	r.Header.Set("Content-Type", mimetype)
	w := httptest.NewRecorder()
	testDB := openTestDatabase(t)
	handler := UploadHandler(testDB)
	handler.ServeHTTP(w, r)
	//check the result
	if w.Code != http.StatusOK {
		t.Fatal("call returned status: ", w.Code, " and message: ", w.Body.String())
	}
	vids, err := testDB.GetVideos()
	if err != nil {
		t.Fatal("failed to get videos to check: ", err)
	}
	if len(vids) == 0 {
		t.Fatal("no videos returned from the database")
	}
	found := false
	for _, v := range vids {
		if v.Name == video.Name {
			found = true
			break
		}
	}
	if !found {
		t.Log(vids)
		t.Fatal("failed to find video with the right name")
	}
	for _, v := range vids {
		os.Remove(filepath.Join(testFileStoreDir, v.StorageFileName()))
		os.Remove(filepath.Join(testFileStoreDir, v.Thumbnail64File))
		os.Remove(filepath.Join(testFileStoreDir, v.Thumbnail128File))
		os.Remove(filepath.Join(testFileStoreDir, v.Thumbnail256File))
	}
}
