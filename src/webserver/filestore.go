package webserver

import (
	"fmt"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/GentlemanTech/holmes_210915/src/models"
)

var fileStoreDir = "/files"

func SetFileStoreDir(newdir string) {
	fileStoreDir = newdir
}

func storeFile(video models.Video, src io.Reader) (string, error) {
	//check the size
	dstfilename := video.StorageFileName()
	dst, err := os.Create(filepath.Join(fileStoreDir, dstfilename))
	if err != nil {
		return "", fmt.Errorf("failed to create destination file: %w", err)
	}
	defer dst.Close()
	n, err := io.Copy(dst, src)
	if err != nil {
		return "", fmt.Errorf("failed to copy file contents to destination file: %w", err)
	}
	fmt.Println("copied", n, "bytes to file")
	dst.Close()
	return dstfilename, nil
}
