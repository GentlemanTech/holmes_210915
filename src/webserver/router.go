package webserver

import (
	"encoding/json"
	"fmt"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/GentlemanTech/holmes_210915/src/database"
	"gitlab.com/GentlemanTech/holmes_210915/src/models"
)

func router(db *database.VideoData, assetsdir string) http.Handler {
	mux := http.NewServeMux()
	mux.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir(assetsdir))))
	mux.Handle("/video/", VideoPageHandler(db))
	mux.Handle("/thumbnail/", ThumbnailHandler(db))
	mux.Handle("/upload/", UploadPageHandler(db))
	mux.Handle("/file_upload/", mwCheckPost(UploadHandler(db)))
	mux.Handle("/", IndexPageHandler(db))
	fmt.Println("created routes successfully")
	return mwLogger(mux)
}

func mwLogger(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("received request for: %s\n", r.URL.String())
		handler.ServeHTTP(w, r)
	})
}

func mwCheckPost(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			fmt.Printf("rejected %s request for url: %s\n", r.Method, r.URL.String())
			return
		}
		handler.ServeHTTP(w, r)
	})
}

func IndexPageHandler(db *database.VideoData) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		data := make(map[string]string)
		videos, err := db.GetVideos()
		if err != nil {
			data["error"] = fmt.Sprintf("failed to get videos: %s", err.Error())
		} else {
			vd, err := json.Marshal(videos)
			if err != nil {
				data["error"] = fmt.Sprintf("failed to marshal videos to json: %s", err.Error())
			}
			data["videos"] = string(vd)
		}
		tmps.ExecuteTemplate(w, "index.gohtml", data)
	})
}

func VideoPageHandler(db *database.VideoData) http.Handler {
	//not actually a page handler, because we just hand the file back
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		params := strings.Split(r.URL.Path, "/")
		if len(params) != 3 {
			fmt.Println("bad number of parameters: ", params)
			http.Error(w, "bad paramters", http.StatusBadRequest)
			return
		}
		vID, err := uuid.Parse(params[2])
		if err != nil {
			fmt.Println("bad video ID: ", params)
			http.Error(w, "bad video ID", http.StatusBadRequest)
			return
		}
		video, err := db.GetVideo(vID)
		if err != nil {
			fmt.Println("failed to get video details: ", err.Error())
			http.Error(w, "bad video", http.StatusBadRequest)
			return
		}
		vfn := filepath.Join(fileStoreDir, video.StorageFileName())
		fmt.Println("serving video file: ", vfn)
		http.ServeFile(w, r, vfn)
	})
}

func ThumbnailHandler(db *database.VideoData) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		params := strings.Split(r.URL.Path, "/")
		if len(params) != 3 {
			fmt.Println("bad number of parameters: ", params)
			http.Error(w, "bad paramters", http.StatusBadRequest)
			return
		}
		tf := params[2]
		http.ServeFile(w, r, filepath.Join(fileStoreDir, tf))
	})
}
func UploadPageHandler(db *database.VideoData) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		data := make(map[string]string)
		cats, err := db.GetCategoriesForUI()
		if err != nil {
			data["error"] = err.Error()
		}
		data["categories"] = string(cats)
		tmps.ExecuteTemplate(w, "upload.gohtml", data)
	})
}

func UploadHandler(db *database.VideoData) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		maxmem := int64(1024 * 1024 * 220) //200Mb max size of video +10% leeway, might as well allow it to do this in one gulp
		err := r.ParseMultipartForm(maxmem)
		if err != nil {
			//TODO: better error handling
			http.Error(w, fmt.Sprintf("failed to upload video: %s", err.Error()), http.StatusUnprocessableEntity)
			return
		}
		for _, fh := range r.MultipartForm.File["file"] {
			if fh.Size > 1024*1024*200 {
				http.Error(w, "video file is too large", http.StatusBadRequest)
				return
			}
			vid := models.Video{
				ID:       uuid.New(),
				Filename: fh.Filename,
				Name:     getField(r.MultipartForm, "title"),
				Category: getField(r.MultipartForm, "category"),
			}
			src, err := fh.Open()
			if err != nil {
				http.Error(w, fmt.Sprintf("failed to open video: %s", err.Error()), http.StatusUnprocessableEntity)
			}
			filename, err := storeFile(vid, src)
			if err != nil {
				//TODO: better error handling
				http.Error(w, fmt.Sprintf("failed to upload video: %s", err.Error()), http.StatusUnprocessableEntity)
				return
			}
			err = vid.GenerateThumbnails(fileStoreDir)
			if err != nil {
				http.Error(w, fmt.Sprintf("failed to generate thumbnails: %s", err.Error()), http.StatusUnprocessableEntity)
				os.Remove(filepath.Join(fileStoreDir, filename))
				return
			}
			err = db.AddVideo(vid)
			if err != nil {
				http.Error(w, fmt.Sprintf("failed to store video in the database: %s", err.Error()), http.StatusUnprocessableEntity)
				os.Remove(filepath.Join(fileStoreDir, filename))
				os.Remove(filepath.Join(fileStoreDir, vid.Thumbnail64File))
				os.Remove(filepath.Join(fileStoreDir, vid.Thumbnail128File))
				os.Remove(filepath.Join(fileStoreDir, vid.Thumbnail256File))
				return
			}
		}
		fmt.Printf("successfully uploaded %d file(s)\n", len(r.MultipartForm.File["file"]))
		w.Write([]byte("OK"))
	})
}

func getField(form *multipart.Form, field string) string {
	fvs := form.Value[field]
	return strings.Join(fvs, "")
}
