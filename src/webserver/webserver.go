package webserver

import (
	"fmt"
	"net/http"

	"gitlab.com/GentlemanTech/holmes_210915/src/database"
)

func Run(db *database.VideoData, addr, templatedir, assetsdir string) error {
	err := LoadPageTemplates(templatedir)
	if err != nil {
		//check the directory - might be different
		return fmt.Errorf("failed to load templates: %w", err)
	}
	return http.ListenAndServe(addr, router(db, assetsdir))
}
