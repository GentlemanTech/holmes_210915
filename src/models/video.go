package models

import (
	"fmt"
	"image"
	"image/png"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/google/uuid"
	"golang.org/x/image/draw"
)

type Video struct {
	ID               uuid.UUID `db:"video_id"`
	Name             string    `db:"name"`
	Filename         string    `db:"filename"`
	Category         string    `db:"category"`
	Thumbnail64File  string    `db:"thumb64filename"`
	Thumbnail128File string    `db:"thumb128filename"`
	Thumbnail256File string    `db:"thumb256filename"`
}

type VideoList []Video

func (vd *Video) GenerateThumbnails(outputdir string) error {
	src := filepath.Join(outputdir, vd.StorageFileName())
	tname := filepath.Join(outputdir, getSizedThumbnailName(vd.StorageFileName(), 100))
	//use ffmpeg to generate the video thumbnail from 1s into the file
	cmd := exec.Command("ffmpeg", "-ss", "00:00:01", "-i", src, "-vframes", "1", "-y", tname)
	errout := &strings.Builder{}
	cmd.Stderr = errout
	fmt.Println("command is: ", cmd.String())
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("failed to create thumbnail file because: %w, stderr was: %s", err, errout.String())
	}
	//then resize it to the various other sizes
	vd.Thumbnail64File, err = createThumbnail(tname, 64)
	if err != nil {
		return fmt.Errorf("failed to create %d thumbnail: %w", 64, err)
	}
	vd.Thumbnail128File, err = createThumbnail(tname, 128)
	if err != nil {
		return fmt.Errorf("failed to create %d thumbnail: %w", 128, err)
	}
	vd.Thumbnail256File, err = createThumbnail(tname, 256)
	if err != nil {
		return fmt.Errorf("failed to create %d thumbnail: %w", 256, err)
	}
	//remove the "100" thumbnail as we don't need it any more
	os.Remove(tname)
	return nil
}

func createThumbnail(thumbnailname string, size int) (string, error) {
	result := strings.ReplaceAll(thumbnailname, ".100.png", fmt.Sprintf(".%d.png", size))
	input, err := os.Open(thumbnailname)
	if err != nil {
		return "", fmt.Errorf("failed to open thumbnail file: %w", err)
	}
	defer input.Close()
	os.Remove(result)
	output, err := os.Create(result)
	if err != nil {
		return "", fmt.Errorf("failed to create thumbnail file %s with: %w", result, err)
	}
	defer output.Close()
	img, err := png.Decode(input)
	if err != nil {
		return "", fmt.Errorf("failed to decode thumbnail file: %w", err)
	}
	input.Close()
	thumbnail := image.NewRGBA(image.Rect(0, 0, size, size))
	draw.BiLinear.Scale(thumbnail, thumbnail.Rect, img, img.Bounds(), draw.Over, nil)
	err = png.Encode(output, thumbnail)
	if err != nil {
		return "", fmt.Errorf("failed to write thumbnail file %s with: %w", result, err)
	}
	output.Close()
	return filepath.Base(result), nil
}

func getSizedThumbnailName(name string, size int) string {
	return fmt.Sprintf("%s.%d.png", filepath.Base(name), size)
}

func (vd *Video) StorageFileName() string {
	return fmt.Sprintf("%s%s", vd.ID.String(), filepath.Ext(vd.Filename))
}
