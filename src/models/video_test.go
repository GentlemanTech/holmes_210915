package models

import (
	"image/png"
	"os"
	"path/filepath"
	"testing"
)

func TestVideoThumbnail(t *testing.T) {
	testdatadir := "../../testdata"
	tv := Video{
		Name:     "badgers",
		Filename: filepath.Join(testdatadir, "badgers.mp4"),
		Category: "funny",
	}
	err := tv.GenerateThumbnails(testdatadir)
	if err != nil {
		t.Fatal(err.Error())
	}
	//we should see 3 png files in the testdata dir
	t64 := filepath.Join(testdatadir, tv.Thumbnail64File)
	f, err := os.Open(t64)
	if err != nil {
		t.Fatal(err.Error())
	}
	defer f.Close()
	img, err := png.Decode(f)
	if err != nil {
		t.Fatal("not a png, decode errored with: ", err)
	}
	if img.Bounds().Size().X != 64 {
		t.Error("width is not 64 but:", img.Bounds().Size().X)
	}
	if img.Bounds().Size().Y != 64 {
		t.Error("height is not 64 but:", img.Bounds().Size().X)
	}
}
