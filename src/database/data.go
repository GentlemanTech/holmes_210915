package database

import (
	"database/sql"
	"encoding/json"
	"fmt"

	"github.com/google/uuid"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
	"gitlab.com/GentlemanTech/holmes_210915/src/models"
)

//VideoData provides a set of functions to fetch/add data
//   the point of this is to separate concerns:
//   everything dealing with how the data is stored needs to be in this module
type VideoData struct {
	db *sqlx.DB
}

func ConnectToDB(host, dbname string) (*VideoData, error) {
	db, err := sqlx.Connect("pgx", fmt.Sprintf("postgresql://%s:%s@%s:%s/%s", "localdev", "password", "localhost", "5432", dbname))
	if err != nil {
		return nil, fmt.Errorf("failed to connect to database: %w", err)
	}
	result := &VideoData{
		db: db,
	}
	checkCategories(result)
	fmt.Println("connected to database successfully")
	return result, nil
}

func (vd *VideoData) GetVideos() (models.VideoList, error) {
	result := models.VideoList{}
	rows, err := vd.db.Queryx("select * from vw_videos_with_categories")
	if err != nil {
		return result, fmt.Errorf("failed to run video selection query: %w", err)
	}
	for rows.Next() {
		v := models.Video{}
		err = rows.StructScan(&v)
		if err != nil {
			return result, fmt.Errorf("failed to scan video collection result: %w", err)
		}
		result = append(result, v)
	}
	return result, nil
}

func (vd *VideoData) GetVideo(id uuid.UUID) (models.Video, error) {
	row := vd.db.QueryRowx("select * from vw_videos_with_categories where video_id = $1", id)
	result := models.Video{}
	err := row.StructScan(&result)
	if err != nil {
		return result, fmt.Errorf("failed to scan video record: %w", err)
	}
	return result, nil
}

func (vd *VideoData) GetCategoriesForUI() (json.RawMessage, error) {
	row := vd.db.QueryRow("select to_json(array_agg(categories.name)) from categories")
	var result json.RawMessage
	err := row.Scan(&result)
	if err != nil {
		if err == sql.ErrNoRows {
			fmt.Println("there are no categories in the database")
			return result, nil
		}
		return result, fmt.Errorf("failed to get the list of categories: %w", err)
	}
	return result, nil
}

func (vd *VideoData) AddVideo(video models.Video) error {
	row := vd.db.QueryRow("select id from categories where name like $1", video.Category)
	var categoryID uuid.UUID
	err := row.Scan(&categoryID)
	if err != nil {
		if err == sql.ErrNoRows {
			//this category doesn't exist in the database
			return fmt.Errorf("category '%s' doesn't exist in the database", video.Category)
		}
		return fmt.Errorf("failed to get category ID for category '%s' because: %w", video.Category, err)
	}
	_, err = vd.db.Exec(
		"insert into videos (id, name, filename, category_id, thumb64filename, thumb128filename, thumb256filename) values ($1,$2,$3,$4,$5,$6,$7)",
		video.ID,
		video.Name,
		video.Filename,
		categoryID,
		video.Thumbnail64File,
		video.Thumbnail128File,
		video.Thumbnail256File,
	)
	if err != nil {
		return fmt.Errorf("failed to insert new record for video into videos table: %w", err)
	}
	return nil
}

func checkCategories(db *VideoData) {
	//check we have some categories, and if not then generate them
	_, err := db.GetCategoriesForUI()
	if err != nil {
		if err == sql.ErrNoRows {
			//add the default categories
			db.db.Exec("insert into categories(name) values ('exercise'")
			db.db.Exec("insert into categories(name) values ('education'")
			db.db.Exec("insert into categories(name) values ('recipe'")
		}
	}
}
