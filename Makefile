run_database:
	-mkdir -p testdata/db
	docker run --name dataserver -e POSTGRES_PASSWORD=password -d --mount type=bind,src=$(shell pwd)/testdata/db,dst=/var/lib/postgresql/data  --mount type=bind,src=$(shell pwd)/data,dst=/var/localdev --network=host postgres

run_migrations:
	docker exec -it -w /var/localdev -e PGPASSWORD=password dataserver psql -U postgres -f /var/localdev/init.sql
	docker exec -it -w /var/localdev -e PGPASSWORD=password dataserver psql -U localdev -d localdev -f /var/localdev/migration_0.deploy.sql
	docker exec -it -w /var/localdev -e PGPASSWORD=password dataserver psql -U localdev -d localtest -f /var/localdev/migration_0.deploy.sql

kill_database: stop_database
	docker container rm dataserver

connect_database:
	docker exec -it -w /var/localdev -e PGPASSWORD=password dataserver psql -d localdev -U postgres

sleep2s:
	sleep 2s

dev_database: kill_database run_database sleep2s connect_database

stop_database:
	-docker container stop dataserver

run:
	go build -o webserver.bin ./src
	./webserver.bin