begin;

-- migrations table
-- I doubt we'll need more than this one migration, but it's simple to do and avoids pain
create table migrations (
	id integer unique,
	created timestamptz not null default now()
);
-- this triggers the "unique" constraint if we've already done this migration
--    which aborts the transaction and reports an error (which we can ignore)
--    to re-run a migration, delete the appropriate id
--    there is no "down" migration - I found they never work right and regularly mess up the data
insert into migrations (id) values (0);

-- we need this extension to allow gen_random_uuid
--    looks like this got pulled into main in v13 and we don't need the extension any more
-- create extension pgcrypto;

--create the categories table
--   because this is 1:1 for the videos table we could be just adding this
--   as a column in that table
--   but we need to enforce referential integrity, so we'll add this here
create table categories (
	id uuid primary key default gen_random_uuid(),
	name text
);

-- video table, used for storing video info
--   using uuids for the primary key means we don't have to police names/filenames being unique
--   because the spec clearly says we only need the 3 resolutions for the thumbnails, we're not
--   going to normalise them. If there was any possibility of this not being the case, I'd
--   be tempted to do something like: create table thumbnails (video_id, size, filename);
create table videos (
	id uuid primary key default gen_random_uuid(),
	name text not null,
	filename text not null,
	category_id uuid references categories(id) not null,
	thumb64filename text,
	thumb128filename text,
	thumb256filename text
);

--add an index on name because we'll need this a lot
create index idx_video_name on videos(name);

-- add a view for fetching videos so we can denormalise categories
--    this is the thing that the Go struct maps to
create view vw_videos_with_categories as
	select
		videos.id as video_id,
		videos.name as name,
		videos.filename as filename,
		categories.name as category,
		videos.thumb64filename as thumb64filename,
		videos.thumb128filename as thumb128filename,
		videos.thumb256filename as thumb256filename
	from
		videos
		join categories on videos.category_id = categories.id
	order by
		videos.name asc
;

commit;